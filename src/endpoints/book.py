from fastapi import APIRouter, Depends, Request
from fastapi.templating import Jinja2Templates
from src.core.dependencies.main import get_templates

router = APIRouter(
    prefix="/books",
    tags=["Books"],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def read_root():
    return {"status": "You have reached book root route"}


@router.get("/list")
async def list_books(
    request: Request, templates: Jinja2Templates = Depends(get_templates)
):
    name = "Test User"
    return templates.TemplateResponse(
        "home_page.html",
        context={
            "request": request,
            "name": name,
            "book_result": ["Harry Potter", "Sherlock Holmes"],
        },
    )
