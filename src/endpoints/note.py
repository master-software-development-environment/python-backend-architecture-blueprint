from typing import List

from fastapi import APIRouter, Depends, Request
from sqlalchemy.orm import Session

from src.core.dependencies.main import get_db_session
from src.database import crud, schemas, models

router = APIRouter(
    prefix="/notes",
    tags=["Notes"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[schemas.Note])
async def read_notes(request: Request, skip: int = 0, limit: int = 100):
    query = """SELECT * FROM notes"""
    rows = await request.state.db.fetch_all(query=query)
    return rows


@router.post("/", response_model=List[schemas.Note])
async def create_note(request: Request, note: schemas.NoteIn):
    query = "INSERT INTO notes(text, completed) VALUES (:text, :completed)"
    values = [
        {"text": note.text, "completed": note.completed},
        # {"text": "New2", "completed": False},
        # {"text": "New3", "completed": False},
    ]
    await request.state.db.execute_many(query=query, values=values)
    query = """SELECT * FROM notes ORDER BY id DESC LIMIT 1"""
    rows = await request.state.db.fetch_all(query=query)
    return rows
