from typing import List

from fastapi import APIRouter, Request

from src.database import schemas

from src.database.tables import ovens

router = APIRouter(
    prefix="/ovens",
    tags=["Ovens"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[schemas.Oven])
async def read_ovens(request: Request, skip: int = 0, limit: int = 100):
    query = ovens.select()
    return await request.state.db.fetch_all(query)


@router.post("/", response_model=schemas.Oven)
async def create_oven(request: Request, oven: schemas.OvenIn):
    query = ovens.insert().values(name=oven.name, company=oven.company)
    last_record_id = await request.state.db.execute(query)
    return {**oven.dict(), "id": last_record_id}
