from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from src.core.dependencies.main import get_db_session
from src.database import crud, schemas

router = APIRouter(
    prefix="/items",
    tags=["Items"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[schemas.Item])
def read_items(skip: int = 0, limit: int = 100, db: Session = Depends(get_db_session)):
    items = crud.get_items(db, skip=skip, limit=limit)
    return items


@router.post("/", response_model=schemas.Item)
async def create_item(
    item: schemas.ItemCreate, user_id: int = 0, db: Session = Depends(get_db_session)
):
    return crud.create_user_item(db=db, user_id=user_id, item=item)
