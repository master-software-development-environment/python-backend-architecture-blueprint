from fastapi import APIRouter, Depends, Request
from fastapi.templating import Jinja2Templates
from src.core.dependencies.main import get_templates

router = APIRouter(
    prefix="/animals",
    tags=["Animals"],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def read_root():
    return {"status": "You have reached animal root route"}


@router.get("/list")
async def list_animals(
    request: Request, templates: Jinja2Templates = Depends(get_templates)
):
    name = "Test User"
    return templates.TemplateResponse(
        "home_page.html",
        context={"request": request, "name": name, "animal_result": ["cat", "dog"]},
    )
