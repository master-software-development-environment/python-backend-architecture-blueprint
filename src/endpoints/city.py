from typing import List

from fastapi import APIRouter, Depends, Request
from sqlalchemy.ext.asyncio import AsyncSession

from src.core.dependencies.main import get_async_db_session
from src.database import crud, schemas, models

router = APIRouter(
    prefix="/cities",
    tags=["Cities"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[schemas.City])
async def read_cities(
    skip: int = 0, limit: int = 100, db: AsyncSession = Depends(get_async_db_session)
):
    cities = await crud.get_cities(db, skip=skip, limit=limit)
    return cities


@router.post("/", response_model=schemas.City)
async def create_city(
    city: schemas.CityIn, db_session: AsyncSession = Depends(get_async_db_session)
):
    return await crud.create_city(db_session=db_session, city=city)
