"""
This is an app file
"""

from fastapi import FastAPI, Request, Form

import strawberry
import sqlalchemy
from strawberry.asgi import GraphQL

from src.graphql.main import graphql_schema

from src.core.middlewares.http.Interceptor import Interceptor
from src.core.dependencies.main import get_templates
from src.core.environment.config import settings

from src.services.main import sample_input
from src.core.logging.log_config_code import init_logger_with_code
from src.core.logging.log_config_dict import init_logger_with_dict
from src.models.sample_input import SampleInput

from src.routes.api import router as api_router
from src.database import models, db

if settings.logging_configuration == 'code':
    logger = init_logger_with_code(__name__)
else:
    logger = init_logger_with_dict(__name__)

## ------------------- Moved to Alembic [MTA] ------------------- ##

# models.Base.metadata.create_all(bind=db.engine)
# db.metadata_for_engine2.create_all(db.engine2)

## ------------------- Moved to Alembic [MTA] ------------------- ##


graphql_app = GraphQL(graphql_schema)

app = FastAPI()
app.add_route("/graphql", graphql_app)
app.add_websocket_route("/graphql", graphql_app)
app.include_router(api_router)


templates = get_templates()


logger.info("FastAPI server has started")


# This is one way of adding http middleware. It is extracted out to a class named Interceptor for more flexibility and control.

# @app.middleware("http")
# async def log_requests(request: Request, call_next):
#     idem = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
#     logger.info(f"rid={idem} start request path={request.url.path}")
#     start_time = time.time()

#     response = await call_next(request)

#     process_time = (time.time() - start_time) * 1000
#     formatted_process_time = '{0:.2f}'.format(process_time)
#     logger.info(f"rid={idem} completed_in={formatted_process_time}ms status_code={response.status_code}")

#     return response

app.add_middleware(Interceptor, logger_attribute=logger, db_attribute=db.database)


@app.on_event("startup")
async def startup():
    await db.database.connect()
## ------------------- Moved to Alembic [MTA] ------------------- ##

    # async with db.async_engine.begin() as conn:
    #     await conn.run_sync(db.Base.metadata.drop_all)
    #     await conn.run_sync(db.Base.metadata.create_all)
    
## ------------------- Moved to Alembic [MTA] ------------------- ##


@app.on_event("shutdown")
async def shutdown():
    await db.database.disconnect()


@app.get("/")
def read_root(request: Request):
    """This is a app root handler"""
    name = "Test User"
    return templates.TemplateResponse(
        "home_page.html", context={"request": request, "name": name}
    )


@app.post("/month")
async def process_month(request: Request, month_input: str = Form()):
    """This is a app process month handler"""
    name = "Test User"
    sample_output = sample_input(month_input)
    return templates.TemplateResponse(
        "home_page.html",
        context={"request": request, "name": name, "result": sample_output["month"]},
    )
