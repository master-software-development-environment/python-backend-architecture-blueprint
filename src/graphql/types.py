import strawberry


@strawberry.type
class Car:
    name: str
    manufacturer: str


@strawberry.type
class Bike:
    model: str
    brand: str


@strawberry.type
class Speaker:
    item_type: str
    item_model: str


@strawberry.type
class Laptop:
    name: str
    touch: bool
    gaming: bool
