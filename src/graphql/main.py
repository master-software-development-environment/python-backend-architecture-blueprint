import strawberry
from src.graphql.schema import RootMutation, RootQuery


graphql_schema = strawberry.Schema(query=RootQuery, mutation=RootMutation)
