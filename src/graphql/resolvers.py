from src.graphql.types import Bike, Car, Laptop, Speaker


def get_cars():
    return [
        Car(
            name="Veyron",
            manufacturer="Bugatti",
        ),
    ]


def get_bikes():
    return [
        Bike(
            model="Thunderbird",
            brand="Royal Enfield",
        ),
    ]


def get_speakers():
    return [
        Speaker(
            item_type="Headphone",
            item_model="Boat Rockerzz",
        ),
    ]


def get_laptops():
    return [
        Laptop(
            name="Dell Vostro",
            touch=True,
            gaming=False,
        ),
    ]
