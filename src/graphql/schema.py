from typing import List
import strawberry

from src.graphql.types import Bike, Car, Laptop, Speaker
from src.graphql.resolvers import get_cars, get_bikes, get_laptops, get_speakers

## Queries


@strawberry.type
class AutomotivesQuery:
    cars: List[Car] = strawberry.field(resolver=get_cars)
    bikes: List[Bike] = strawberry.field(resolver=get_bikes)


@strawberry.type
class ElectronicsQuery:
    speakers: List[Speaker] = strawberry.field(resolver=get_speakers)
    laptops: List[Laptop] = strawberry.field(resolver=get_laptops)


@strawberry.type
class RootQuery(AutomotivesQuery, ElectronicsQuery):
    pass


## Mutations


@strawberry.type
class AutomotivesMutation:
    @strawberry.mutation
    def add_car(self, name: str, manufacturer: str) -> Car:
        print(f"Adding car {name} by {manufacturer}")
        return Car(name=name, manufacturer=manufacturer)

    @strawberry.mutation
    def add_bike(self, model: str, brand: str) -> Bike:
        print(f"Adding bike {model} by {brand}")
        return Bike(model=model, brand=brand)


@strawberry.type
class ElectronicsMutation:
    @strawberry.mutation
    def add_speaker(self, item_type: str, item_model: str) -> Speaker:
        print(f"Adding speaker {item_type} by {item_model}")
        return Speaker(item_type=item_type, item_model=item_model)

    @strawberry.mutation
    def add_laptop(self, name: str, touch: bool, gaming: bool) -> Laptop:
        print(f"Adding laptop {name} with touch: {touch} and gaming: {gaming}")
        return Laptop(name=name, touch=touch, gaming=gaming)


@strawberry.type
class RootMutation(AutomotivesMutation, ElectronicsMutation):
    pass
