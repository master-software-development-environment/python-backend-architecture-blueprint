from pydantic import BaseModel


class SampleInput(BaseModel):  # pylint: disable=too-few-public-methods
    """This is a SampleInput pydantic class"""

    month: str
