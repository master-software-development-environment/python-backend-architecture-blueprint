from fastapi import APIRouter

from src.endpoints import animal, book, user, item, note, city, oven

router = APIRouter()
router.include_router(animal.router)
router.include_router(book.router)
router.include_router(user.router)
router.include_router(item.router)
router.include_router(note.router)
router.include_router(city.router)
router.include_router(oven.router)
