"""Initial DB setup

Revision ID: bd12ec08903b
Revises: 
Create Date: 2023-05-10 10:46:01.363672

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bd12ec08903b'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table( # This tells Alembic that, when upgrading, ovens table needs to be created.
        "ovens", # The name of the table.
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String),
        sa.Column("company", sa.String),            
    )


def downgrade() -> None:
    op.drop_table("ovens")
