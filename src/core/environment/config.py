from pydantic import BaseSettings
from pathlib import Path
import os


class Settings(BaseSettings):
    db_type:str
    postgres_db_name:str
    postgres_db_user:str
    postgres_db_password:str
    postgres_db_host:str
    sqlite_db_name: str
    logging_configuration: str

    class Config:
        env_file = Path(__file__).resolve().parent.parent.parent.parent  / 'envs' / f'{os.getenv("STAGE") if os.getenv("STAGE") is not None else ""}.env'

settings = Settings()