import random
import string
import time

from fastapi import Request
from starlette.middleware.base import BaseHTTPMiddleware


class Interceptor(BaseHTTPMiddleware):
    def __init__(self, app, logger_attribute: str, db_attribute):
        super().__init__(app)
        self.logger = logger_attribute  # Optional
        self.db = db_attribute  # Optional

    async def dispatch(self, request: Request, call_next):
        idem = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
        self.logger.info(f"rid={idem} start request path={request.url.path}")
        if request.url.path.startswith("/notes/") or request.url.path.startswith(
            "/ovens/"
        ):
            self.logger.info("Intercepting notes or ovens route")
            request.state.db = self.db
        start_time = time.time()

        response = await call_next(request)

        process_time = (time.time() - start_time) * 1000
        formatted_process_time = "{0:.2f}".format(process_time)
        self.logger.info(
            f"rid={idem} completed_in={formatted_process_time}ms status_code={response.status_code}"
        )

        return response
