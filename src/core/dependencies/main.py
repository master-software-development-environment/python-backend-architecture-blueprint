from pathlib import Path

from fastapi.templating import Jinja2Templates

from src.database.db import SessionLocal, AsyncSessionLocal
from src.core.environment.config import Settings

BASE_DIR = Path(__file__).resolve().parent.parent.parent


def get_templates():
    return Jinja2Templates(directory=str(Path(BASE_DIR, "templates")))


def get_db_session():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


async def get_async_db_session():
    async with AsyncSessionLocal() as async_session:
        yield async_session


