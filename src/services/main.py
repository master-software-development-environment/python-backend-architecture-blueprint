"""
This is a demo main file
"""


import os.path
from src.utils.demo import imported_fn


def my_function():
    """This is a demo function"""
    x = imported_fn()
    return x


def sample_input(input_month):
    """This is a demo function for FastAPI"""
    print("Entered month is : ", input_month)
    return {"month": input_month, "status": "SUCCESS"}


# pylint: disable-msg=too-many-arguments
def sample_write_file(day, month, year, input_1, input_2, output_folder="output_data"):
    """This is a demo function used for exploring pytest"""
    output_directory_exists = os.path.exists(output_folder)
    if output_directory_exists is True:
        file_exists = os.path.exists(output_folder + "/" + month + "_" + year)
        if file_exists:
            with open(
                output_folder + "/" + month + "_" + year, "a", encoding="utf8"
            ) as f:
                f.write(f"{day} | {input_1} | {input_2}\n")
        else:
            with open(
                output_folder + "/" + month + "_" + year, "w", encoding="utf8"
            ) as f:
                f.write("====Values are stored here====")
    else:
        os.makedirs(output_folder)


def sample_read_file(month, year, output_folder="output_data"):
    with open(output_folder + "/" + month + "_" + year, "r", encoding="utf8") as f:
        lines = f.read().splitlines()
    return lines


# pylint: enable-msg=too-many-arguments


# pylint: disable-msg=too-many-arguments
def write(month, day, current_day, year, rating, data="output_data"):
    """This is a demo function used for exploring pytest"""
    directory_exists = os.path.exists(data + "/" + year)
    temp = day
    if directory_exists is True:
        file_exists = os.path.exists(data + "/" + year + "/" + month)
        if file_exists is True:
            with open(data + "/" + year + "/" + month, "a", encoding="utf8") as f:
                f.write(f"{day} | {rating}\n")
                while current_day - temp > 1:
                    f.write(f"{temp + 1} | {0}\n")
                    temp = temp + 1
        else:
            with open(data + "/" + year + "/" + month, "w", encoding="utf8") as f:
                f.write(f"{day} | {rating}\n")
                while current_day - temp > 1:
                    f.write(f"{temp + 1} | {0}\n")
                    temp = temp + 1
    else:
        os.makedirs(data + "/" + year)
        with open(data + "/" + year + "/" + month, "w", encoding="utf8") as f:
            f.write(f"{day} | {rating}\n")
            while current_day - temp > 1:
                f.write(f"{temp + 1} | {0}\n")
                temp = temp + 1


# pylint: enable-msg=too-many-arguments

if __name__ == "__main__":
    # write('March', 28, 30, '2023', 9)
    input_month = input("Enter any month: ")
    sample_input(input_month)
