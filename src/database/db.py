import os
import databases
from pathlib import Path

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from src.core.environment.config import settings

# -------------------------------------------------------#
if settings.db_type == 'sqlite':

    print("######### Inside sqlite handler #########")
    SQLITE_DIR = Path(__file__).resolve().parent.parent.parent
    SQLITE_DB_LOCATION = str(Path(SQLITE_DIR) / 'sqlite' / 'dbs' / settings.sqlite_db_name)
    SQLALCHEMY_DATABASE_URL = f"sqlite:///{SQLITE_DB_LOCATION}"
    SQLALCHEMY_DATABASE_URL_ASYNC = f"sqlite+aiosqlite:///{SQLITE_DB_LOCATION}"
else:
    print("######### Inside PG handler #########")
    SQLALCHEMY_DATABASE_URL = f"postgresql://{settings.postgres_db_user}:{settings.postgres_db_password}@{settings.postgres_db_host}/{settings.postgres_db_name}"
    SQLALCHEMY_DATABASE_URL_ASYNC = f"postgresql+asyncpg://{settings.postgres_db_user}:{settings.postgres_db_password}@{settings.postgres_db_host}/{settings.postgres_db_name}"
# -------------------------------------------------------#

# APPROACH 1 : Database with async support
database = databases.Database(SQLALCHEMY_DATABASE_URL)

# APPROACH 2 : Creating sync engine
if settings.db_type == 'sqlite':
    engine = create_engine(
        SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
    )
else:
    engine = create_engine(
        SQLALCHEMY_DATABASE_URL
    )

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# APPROACH 3 : Creating async engine
async_engine = create_async_engine(SQLALCHEMY_DATABASE_URL_ASYNC, echo=True)
AsyncSessionLocal = sessionmaker(
    async_engine, class_=AsyncSession, expire_on_commit=False
)

# APPROACH 4 : Creating a different sync engine
metadata_for_engine2 = sqlalchemy.MetaData()
if settings.db_type == 'sqlite':    
    engine2 = create_engine(
        SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
    )
else:
    engine2 = create_engine(
        SQLALCHEMY_DATABASE_URL
    )

# -------------------------------------------------------#
Base = declarative_base()
# -------------------------------------------------------#
