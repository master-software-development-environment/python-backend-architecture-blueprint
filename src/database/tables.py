import sqlalchemy
from src.database.db import metadata_for_engine2

ovens = sqlalchemy.Table(
    "ovens",
    metadata_for_engine2,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String),
    sqlalchemy.Column("company", sqlalchemy.String),
)
