#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER test with password 'test123';
	CREATE DATABASE db;
	GRANT ALL PRIVILEGES ON DATABASE db TO test;
EOSQL