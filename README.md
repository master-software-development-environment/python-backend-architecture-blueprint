# FastAPI AIO Blueprint Project

The project if opened in Gitpod environment will automatically open up 3 tabs: 
1. The FastAPI application itself on port 8000 with default database as Sqlite 
2. Cloud DBeaver on port 8978 
3. SQLite Browser on 3000.

The root path of the application serves a minimilastic UI with a certain set of APIs. To interact with the remaining APIs, navigate to the `/docs` route to reach the Swagger endpoint from where other APIs can be accessed.

*All the run instructions for running the project in Gitpod is specified in `.gitpod.yml` file. If the project need to be run outside Gitpod, follow the steps 1-4 mentioned in the `Usage Instructions` section.*

## Tech Stack:

1. **API framework:** FastAPI ASGI web framework using `fastapi`
2. **HTTP server:** Uvicorn for serving a high-performance ASGI server using `uvicorn`
3. **Unit Testing:** Pytest with `pytest` and plugin `pytest-html` for generating HTML reports.
4. **FastAPI client testing:** `httpx` library is required for FastAPI client testing
5. **Linting:** Using `pylint`.
6. **Coverage:** `pytest-cov` for running coverage analysis on the source code. 
7. **UI:** `python-multipart` and `jinja2` for forms and templates
8. **ORM:** `sqlalchemy` for database ORM.
9. **GraphQL:** `strawberry-graphql[debug-server]` for graphql server
10. **Sync DB drivers:** `psycopg2` for sync access to Postgres. Driver for sync access to sqlite db is built into python
11. **Async DB drivers:** `databases[aiosqlite]` for async access to sqlite DB and `databases[asyncpg]` , `databases[aiopg]` for async access to postgres.
12. **Environment variable management:** `python-dotenv` for environment variables loading, to be used with pydantic.
13. **DB change management:** `alembic` for database change management.
14. **Logging:** `logging` library is used for logging. Although fastapi has logging library integrated, external `logging` library is used to ensure there is complete control and customizability. Two variations of logging is created: using `code` and using `dict`. These configurations are defined in `src.core.logging` package. By default logging using `code` is used. This can be switched to logging using `dict` by changing the `LOGGING_CONFIGURATION` switch in the `.env` and/or `prod.env` file (based on the stage the application is being run) from `code` to `dict`.

## Usage instructions:

1. Create virtual environment: `python -m venv master_venv`
2. Activate the environment: For windows, `master_venv\Scripts\activate.bat` and for linux `source master_venv/bin/activate`.
3. Install dependencies: `pip install -r requirements.txt`
4. Run the server, by executing the command `python main.py`. To run in a different environment, like prod, use the command `STAGE='prod' python main.py` where STAGE is the environment variable to pass the build environment like prod, stage, etc (defult value is '', ie, local). This essentially runs `uvicorn src.app:app --reload`.
5. Create the reports folder `mkdir reports`. Lint the codes in src, test and app.py by running `pylint src tests --output-format=json:reports/lint_report.json,colorized`.
6. For running the coverage test, use the command, `pytest --cov --cov-report=html:reports/coverage_reports`.
7. Run all the test cases: `pytest tests -v -s --html=reports/unit_test_results.html`. For executing smoke suite, run `pytest tests -v -s  -m smoke`. For regression suite, `pytest tests -v -s  -m regression --html=report.html`. The `-v` flag is for verbosity and the `-s` flag is for disabling capturing and so all fixture strings will be written to output. NOTE: The coverage and unit tests can be clubbed into a single command, `pytest tests -v -s --html=reports/unit_test_results.html --cov --cov-report=html:reports/coverage_reports`.
8. Prettify the code by running, `black src tests`.

## Notes:

**Environment variables:** The project currently has environment variables defined for `local` and `prod` stages in `.env` and `prod.env` files respectively in the `envs` folder. Pydantic uses the corresponding environment file based on the stage. Running the command `python main.py` would spawn the application in `local` stage and `.env` file would be used for populating the pydantic settings. If the command `STAGE='prod' python main.py` is used, the application would start in `prod` mode and `prod.env` file woud be used as the environment file which in turn would be used by pydantic for populating the settings.

**Alembic:** For handling database changes, alembic is used. After making changes to db models, run `alembic revision --autogenerate -m "Sample title - Table modification"`. This will create the migration script. In order to apply the migration script, run `alembic upgrade head` for SQLite upgrade and `STAGE='prod' alembic upgrade head` for postgres upgrade.

*The ovens table is not automatically handled using alembic as its being created by a second db engine. Therefore, any changes to that table alone need to be manually coded into alembic migration scripts.*

**Venv:** For deactivating the environment, run `master_venv\Scripts\deactivate.bat` for windows and `deactivate` for linux.

**Database:** Four types of database connections are highlighted in this project. Refer `src.database.db` for the different engines created in this project.
1. The `notes` route is serviced by an async database connection `src.database.db.database` without using SQLAlchemy models. This is made possible by intercepting the call to the notes route using an interceptor defined in `src.core.middlewares.http.Interceptor` and passing an async database `connection` as a custom `db` attribute in the `state` attribute of the request object. The db operations are done using native SQL statements without the use of SQLAlchemy models. 
2. All the routes except for the `ovens` and `notes` route is handled using a sync engine named `engine`. The route filtering is handled in the aforementioned `Interceptor`. The db operations are managed using SQLAlchemy models transformed into db operations via the db engine. This approach uses a db `session` dependency which is injected into each route as a `FastAPI dependency` defined in `src.core.dependencies.main`.
3. Async engine can also be created and used to create database objects in the database. Currently, the usage of such an async engine named `async_engine` is commented off in the `startup()` function of `app.py` in favor of alembic.
4. A second sync engine, `engine2`, is created which works using the Tables constructs from SQLAlchemy. The `ovens` route leverages this sync engine. The change management for the ovens table is not handled by alembic and should be taken care of manually since the metadata for this table is registered in `engine2` whereas alembic uses `engine` for change management. This approach is not directly dependant on a `session` object or a `connection` object but directly uses the `ovens` table definition defined in `src.database.tables`.

**Middlewares:** The project currently overrides http middleware only. Two variations of using http middlewares is highlighted in this project. The first one is a straightforward way which is commented off in `app.py` in favor of a more customizable approach implemented using an `Interceptor` class in `src.core.middlewares.http` package.

## Reference links: 

https://nuculabs.dev/p/pytest-fixtures-and-yield <br/>
https://eokulik.com/build-test-suite-with-pytest/ <br/>
https://betterprogramming.pub/understand-5-scopes-of-pytest-fixtures-1b607b5c19ed <br/>
https://levelup.gitconnected.com/unit-testing-in-python-mocking-patching-and-dependency-injection-301280db2fed <br/>
https://pytest-html.readthedocs.io/en/latest/user_guide.html <br/>
https://towardsdatascience.com/how-to-fix-modulenotfounderror-and-importerror-248ce5b69b1c <br/>
https://eugeneyan.com/writing/how-to-set-up-html-app-with-fastapi-jinja-forms-templates/ <br/>
https://dev.to/tomas223/logging-tracing-in-python-fastapi-with-opencensus-a-azure-2jcm <br/>
https://stackoverflow.com/questions/39869793/when-do-i-need-to-use-sqlalchemy-back-populates#39870250 <br/>
https://fastapi.tiangolo.com/tutorial/sql-databases/ <br/>
https://fastapi.tiangolo.com/advanced/async-sql-databases/ <br/>
https://github.com/encode/databases <br/>
https://stackoverflow.com/questions/63270196/how-to-do-persistent-database-connection-in-fastapi <br/>
https://stribny.name/blog/fastapi-asyncalchemy/ <br/>
https://fastapi.tiangolo.com/advanced/settings/ <br/>
https://docs.pydantic.dev/latest/usage/settings/ <br/>
https://hub.docker.com/_/postgres/ <br/>
https://harrisonmorgan.dev/2021/02/15/getting-started-with-fastapi-users-and-alembic/<br/>