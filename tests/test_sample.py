"""
This is a demo test file
"""


from unittest.mock import patch, MagicMock, mock_open
import pytest
from src.services.main import my_function, write, sample_write_file, sample_read_file


def mock_imported_fn():
    """This is a demo mock function"""
    return 3


@pytest.fixture(scope="class")
def dummy_data():
    """This is a fixture function at class level"""
    print("\nthis part will work before every test class")
    yield None
    print("\nthis part will work after every test class")


class TestOne:
    """This is first test class"""

    @pytest.fixture(scope="function")
    def demo(self):
        """This is a fixture function at function level"""
        print("\nthis part will work before every test function")
        yield None
        print("\nthis part will work after every test function")

    # pylint: disable-msg=unused-argument
    @pytest.mark.regression
    def test_first(self, demo):
        """This is test_first"""
        assert True

    # pylint: enable-msg=unused-argument
    @pytest.mark.smoke
    def test_second(self):
        """This is test_second"""
        assert True

    # @pytest.mark.parametrize(
    #     'my_num',
    #     [2, 3, 4]
    # )
    # def test_third(self,my_num):
    #     assert my_num == 2


@pytest.mark.usefixtures("dummy_data")
class TestSecond:
    """This is second test class"""

    @pytest.mark.regression
    def test_third(self):
        """This is test_third"""
        assert True

    def test_fourth(self):
        """This is test_fourth"""
        assert True


class TestThird:
    """This is third test class"""

    # Mocking with builtin library - unittests
    @patch("src.services.main.imported_fn", mock_imported_fn)
    def test_fifth(self):
        """This is test_fifth"""
        assert my_function() == 3

    def test_sixth(self):
        """This is test_sixth"""
        with patch("src.services.main.imported_fn", mock_imported_fn):
            y = my_function()
        assert y == 3

    # Mocking with external library - pytest
    def test_seventh(self, monkeypatch):
        """This is test_seventh"""
        monkeypatch.setattr("src.services.main.imported_fn", mock_imported_fn)
        assert my_function() == 3


class TestFourth:
    """This is fourth test class"""

    # Mocking with unittest patch
    def test_sample_read_file_with_unittest_patch(self):
        """This is test_sample_read_file_with_unittest_patch"""
        test_path = "test_output_data"
        with patch("builtins.open", mock_open(read_data="==Header==\n")) as mock_file:
            lines_output = sample_read_file("March", "2023", test_path)
            mock_file.assert_called_once_with(
                test_path + "/March_2023", "r", encoding="utf8"
            )
            assert lines_output[0] == "==Header=="

    # Mocking with monkeypatch
    def test_sample_read_file_with_monkeypatch(self, monkeypatch):
        """This is test_sample_read_file_with_monkeypatch"""
        test_path = "test_output_data"
        mock_file = mock_open(read_data="==Header==\n")
        monkeypatch.setattr("builtins.open", mock_file)
        lines_output = sample_read_file("March", "2023", test_path)
        mock_file.assert_called_once_with(
            test_path + "/March_2023", "r", encoding="utf8"
        )
        assert lines_output[0] == "==Header=="

    def test_sample_write_directory_not_exists(self, monkeypatch):
        """This is test_sample_write_directory_not_exists"""
        test_path = "test_output_data"
        mock_file = mock_open()
        monkeypatch.setattr("builtins.open", mock_file)
        path_exists_check_mocks = [False]
        path_mock = MagicMock(side_effect=path_exists_check_mocks)
        monkeypatch.setattr("os.path.exists", path_mock)
        mock_mkdirs = MagicMock()
        monkeypatch.setattr("os.makedirs", mock_mkdirs)
        sample_write_file("01", "March", "2023", 3, 4, test_path)
        mock_mkdirs.assert_called()

    def test_sample_write_directory_exists_file_not_exists(self, monkeypatch):
        """This is test_sample_write_directory_exists_file_not_exists"""
        test_path = "test_output_data"
        mock_file = mock_open()
        monkeypatch.setattr("builtins.open", mock_file)
        path_exists_check_mocks = [True, False]
        path_mock = MagicMock(side_effect=path_exists_check_mocks)
        monkeypatch.setattr("os.path.exists", path_mock)
        mock_mkdirs = MagicMock()
        monkeypatch.setattr("os.makedirs", mock_mkdirs)
        sample_write_file("01", "March", "2023", 3, 4, test_path)
        mock_file.assert_called_once_with(
            test_path + "/March_2023", "w", encoding="utf8"
        )
        mock_file().write.assert_called_with("====Values are stored here====")
        mock_mkdirs.assert_not_called()

    def test_sample_write_directory_exists_file_exists(self, monkeypatch):
        """This is test_sample_write_directory_exists_file_exists"""
        test_path = "test_output_data"
        mock_file = mock_open()
        monkeypatch.setattr("builtins.open", mock_file)
        path_exists_check_mocks = [True, True]
        path_mock = MagicMock(side_effect=path_exists_check_mocks)
        monkeypatch.setattr("os.path.exists", path_mock)
        mock_mkdirs = MagicMock()
        monkeypatch.setattr("os.makedirs", mock_mkdirs)
        sample_write_file("01", "March", "2023", 3, 4, test_path)
        mock_file.assert_called_once_with(
            test_path + "/March_2023", "a", encoding="utf8"
        )
        mock_file().write.assert_called_with("01 | 3 | 4\n")
        mock_mkdirs.assert_not_called()
