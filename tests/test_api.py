from fastapi.testclient import TestClient
import pytest
from src.core.environment.config import settings

settings.db_type = 'sqlite'
settings.sqlite_db_name = 'sql_app_test.db'

from src.app import app


@pytest.fixture(scope="session")
def client() -> None:    
    yield TestClient(app)


def test_read_book(client):
    response = client.get("/books/")
    assert response.status_code == 200
    assert response.json() == {"status": "You have reached book root route"}

def test_create_user(client):
    response = client.post(
        "/notes/",   
        json={"text": "testtext", "completed": True},
    )
    assert response.status_code == 200
    # assert response.json() == {
    #     "id": 2,
    #     "text": "testtext",        
    #     'completed': True
    # }
